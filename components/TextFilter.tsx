import { FunctionComponent } from "react";

import Image from 'next/image';

import MagnifyingGlass from '../public/magnifying-glass.svg'

type TextFilterProps = {
  label: string
  placeholder: string
}

const TextFilter: FunctionComponent<TextFilterProps> = ({ label, placeholder }) => {
  return (
    <div className="mt-8">
    <label className="relative">
      <span className="font-bold">Usuarios / Perros</span>
      <div className="absolute top-15 left-5">
        <Image
          src={MagnifyingGlass}
          alt="magnifying glass"
          width={18}
          height={18}
        />
      </div>
      <input
        type="text"
        placeholder="Buscar por nombre"
        className="bg-black-primary w-full py-5 pl-14 mt-4 placeholder-white-primary rounded-lg focus:outline-none"
      />
    </label>
  </div>
  )
}

export default TextFilter;