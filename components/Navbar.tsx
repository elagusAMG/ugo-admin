import { FunctionComponent } from 'react'

import { useRouter } from 'next/router'
import Link from 'next/link'
import Image from 'next/image'

import logo from '../public/logo.svg'
import dog from '../public/nav/dog.svg'
import bill from '../public/nav/bill.svg'
import dogStrap from '../public/nav/dogStrap.svg'

const Navbar: FunctionComponent = () => {
  const router = useRouter()

  const isActive = (href: string): string => router.pathname === href ? 'underline text-black-primary' : '';

  return (
    <nav className="h-screen w-36 bg-pink-primary pl-4 pr-4 pt-11 relative">
      <div className="text-center">
        <Link href="/">
          <a>
            <Image src={logo} alt="uGo admin logo" width={82} height={24} />
          </a>
        </Link>
      </div>
      <ul className="mt-16 space-y-5">
        <li>
          <Link href="/">
            <a className={isActive('/')}>
              <Image src={dog} alt="dog" width={15} height={15} />{' '}
              Paseos
            </a>
          </Link>
        </li>
        <li>
          <Link href="/payments">
            <a className={isActive('/payments')}>
              <Image src={bill} alt="bill" width={15} height={15} />{' '}
              Pagos
            </a>
          </Link>
        </li>
        <li>
          <Link href="/walkers">
            <a className={isActive('/walkers')}>
              <Image src={dogStrap} alt="dog strap" width={15} height={15} />{' '}
               Paseadores
            </a>
          </Link>
        </li>
        <li>
          <Link href="/owners">
            <a className={isActive('/owners')}>
              <Image src={dogStrap} alt="dog strap" width={15} height={15} />{' '}
               Dueños
            </a>
          </Link>
        </li>
        <li>
          <Link href="/claims">
            <a className={isActive('/claims')}>
              <Image src={dogStrap} alt="dog strap" width={15} height={15} />{' '}
              Reclamos
            </a>
          </Link>
        </li>
      </ul>
      <div className="text-xs bottom-0 text-black-primary space-y-5 mb-20 absolute">
        <p>v1.0</p>
        <p>Configuración</p>
        <p>Cerrar sesión</p>
      </div>
    </nav>
  )
}

export default Navbar
