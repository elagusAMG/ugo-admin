import { FunctionComponent } from 'react'

import Head from 'next/head'

import Navbar from './Navbar'

const Layout: FunctionComponent = ({ children }) => (
  <>
    <Head>
      <title>uGo! Admin</title>
      <meta name="description" content="Admin for uGo! users" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <div className="flex">
      <Navbar />
      <main>{children}</main>
    </div>
  </>
)

export default Layout
