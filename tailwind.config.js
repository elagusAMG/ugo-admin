module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'], // Tailwind can tree-shake unused styles in production builds
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      inset: {
        '15': '3.8rem'
      }
    },
    colors: {
      black: {
        primary: '#000000',
      },
      white: {
        primary: '#FFFFFF',
      },
      pink: {
        primary: '#F4B5CD',
      },
      gray: {
        primary: '#231F20',
        secondary: '#343031',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
