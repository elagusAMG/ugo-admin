import type { NextPage } from 'next'
import Image from 'next/image'

import TextFilter from '../components/TextFilter'

const Home: NextPage = () => {
  return (
    <div className="h-screen w-80 bg-gray-secondary pl-8 pr-4 pt-9">
      <h1 className="font-bold text-3xl">Paseos</h1>

      <TextFilter label="Usuarios / Perros" placeholder="Buscar por nombre" />

      <TextFilter label="Paseadores" placeholder="Buscar por nombre" />
    </div>
  )
}

export default Home
